<?php
    require_once './config.php';
    session_start();
    require_once './html/header.php'; 
?>
    <center>
        <h1>Přidat kontakt</h1>
        <?php
            if($_SESSION['info'])
            {
                echo $_SESSION['info'];
                $_SESSION['info'] = '';
            }

            else if($_SESSION['info_uspech'])
            {
                echo $_SESSION['info_uspech'];
                $_SESSION['info_uspech'] = '';
            }

            else
            {
                echo 'Vyplň prosím všechny údaje.';
            }
        ?>
        <form method="POST" action="./overeni_n.php">
            <br /><table>
                <tr>
                    <td>Jméno: </td><td><input name="jmeno" type="text" value="<?php if($_SESSION['jmeno']) { echo $_SESSION['jmeno']; $_SESSION['jmeno'] = ''; } ?>" /></td>
                </tr>
                <tr>
                    <td>Příjmení: </td><td><input name="prijmeni" type="text" value="<?php if($_SESSION['prijmeni']) { echo $_SESSION['prijmeni']; $_SESSION['prijmeni'] = ''; } ?>" /></td>
                </tr>
                <tr>
                    <td>Předvolba: </td><td><input name="predvolba" type="text" value="<?php if($_SESSION['predvolba']) { echo $_SESSION['predvolba']; $_SESSION['predvolba'] = ''; } ?>" /></td>
                </tr>
                <tr>
                    <td>Tel. číslo: </td><td><input name="tel_cislo" type="text" value="<?php if($_SESSION['tel_cislo']) { echo $_SESSION['tel_cislo']; $_SESSION['tel_cislo'] = ''; } ?>" /></td>
                </tr>
                <tr>
                    <td></td><td align="right"><input name="tlacitko" type="submit" value="Přidat" /></td>
                </tr>
            </table>
        </form>
        <a href="index.php">Zpět na seznam</a>
    </center>
<?php require_once './html/footer.php'; ?>