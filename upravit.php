<?php
    require_once './config.php';
    session_start();
    require_once './html/header.php'; 
?>
    <center>
        <h1>Upravit kontakt</h1>
        Staré:<br />
        <?php
            if($_GET['id'])
            {
                $id = $_GET['id'];
                $_SESSION['id'] = $id;
                $stare = mysql_query("SELECT * FROM tel_cisla LEFT JOIN kontakty ON tel_cisla.kontaktni_id=kontakty.kontakt_id ");
                echo '<table>';
                while ($zaznam = mysql_fetch_assoc($stare))
                {
                    echo '<tr><td>'.htmlspecialchars($zaznam['jmeno']).'</td>';
                    echo '<td>'.htmlspecialchars($zaznam['prijmeni']).'</td>';
                    echo '<td>'.htmlspecialchars($zaznam['predvolba']).'</td>';
                    echo '<td>'.htmlspecialchars($zaznam['tel_cislo']).'</td></tr>';
                }
                echo '</table>'.'<br />';
            }
            
            else
            {
                echo 'Neplatné ID.'.'<br />';
            }
       
            if($_SESSION['info'])
            {
                echo $_SESSION['info'];
                $_SESSION['info'] = '';
            }

            else if($_SESSION['info_uspech'])
            {
                echo $_SESSION['info_uspech'];
                $_SESSION['info_uspech'] = '';
            }

            else
            {
                echo 'Vyplň prosím všechny údaje.';
            }
            ?>
        
        <form method="POST" action="./overeni_u.php">
            <br /><table>
                Nové:
                <tr>
                    <td>Jméno: </td><td><input name="jmeno" type="text" value="<?php echo $_SESSION['jmeno']; $_SESSION['jmeno'] = ''; ?>" /></td>
                </tr>
                <tr>
                    <td>Příjmení: </td><td><input name="prijmeni" type="text" value="<?php echo $_SESSION['prijmeni']; $_SESSION['prijmeni'] = ''; ?>" /></td>
                </tr>
                <tr>
                    <td>Předvolba: </td><td><input name="predvolba" type="text" value="<?php echo $_SESSION['predvolba']; $_SESSION['predvolba'] = ''; ?>" /></td>
                </tr>
                <tr>
                    <td>Tel. číslo: </td><td><input name="tel_cislo" type="text" value="<?php echo $_SESSION['tel_cislo']; $_SESSION['tel_cislo'] = ''; ?>" /></td>
                </tr>
                <tr>
                    <td></td><td align="right"><input name="tlacitko" type="submit" value="Upravit" /></td>
                </tr>
            </table>
        </form>
        <a href="index.php">Zpět na seznam</a>
    </center>
<?php require_once './html/footer.php'; ?>